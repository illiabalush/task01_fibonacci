/**
 * Copyright (c) Illia Balush
 * This software is the confidential
 * and made for Epam Systems course
 */

package com.balush;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class implements the work of Fibonacci numbers
 *
 * @author Illia Balush
 * @version 1.0
 * @since 2019-03-30
 */

public class Application {
    /**
     * Store the order of the Fibonacci numbers
     * and using it in calculating the percentage of odd
     * and even numbers
     */
    private static ArrayList<Integer> fibonacciNumbers = new ArrayList<Integer>();

    /**
     * Determines whether the number is even or odd
     *
     * @param number the number to be checked
     * @return true or false whether the number is even or odd
     */
    public static boolean isOddNumber(final int number) {
        return number % 2 != 0;
    }

    /**
     * Show odd numbers from start to end of the interval
     * and show even numbers from end to start
     *
     * @param intervalBegin beginning of the interval
     * @param intervalEnd end of the interval
     */
    public static void showOddAndEvenNumbers(final int intervalBegin, final int intervalEnd) {
        // move in cycle from beginning interval to the end each number
        // and if this number is odd then output it
        // show of an even numbers we begin from the end of the interval
        System.out.print("Odd numbers: ");
        for (int i = intervalBegin; i < intervalEnd; i++) {
            if (isOddNumber(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.print("\nEven numbers: ");
        for (int i = intervalEnd; i > intervalBegin; i--) {
            if (!isOddNumber(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * Search for the sum of an odd numbers and even numbers
     *
     * @param intervalBegin the beginning of the searching values
     * @param intervalEnd the end of the searching values
     */
    public static void showSumOddAndEvenNumbers(final int intervalBegin, final int intervalEnd) {
        // creating variables for sum odds and even numbers
        // then moving in the cycle and if this number is odd then add it to sumOfOddNumbers
        // similar with even numbers
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        for (int i = intervalBegin; i < intervalEnd; i++) {
            if (isOddNumber(i)) {
                sumOfOddNumbers += i;
            } else {
                sumOfEvenNumbers += i;
            }
        }
        System.out.println("Sum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);
        System.out.println("General sum: " + (sumOfEvenNumbers + sumOfOddNumbers));
    }

    /**
     * Finds the maximum of an odd number from the interval
     *
     * @param intervalBegin the beginning of the searching values
     * @param intervalEnd the end of the searching values
     * @return max odd value from the interval
     */
    public static int getMaxOddNumberFromInterval(final int intervalBegin, final int intervalEnd) {
        // moving in cycle and if number is bigger than maxOddNumber
        // then we assign the variable maxOddNumber value of that number
        int maxOddNumber = 0;
        for (int i = intervalBegin; i < intervalEnd; i++) {
            if (isOddNumber(i) && i > maxOddNumber) {
                maxOddNumber = i;
            }
        }
        return maxOddNumber;
    }

    /**
     * Finds the maximum of an even number from the interval
     *
     * @param intervalBegin the beginning of the searching values
     * @param intervalEnd the end of the searching values
     * @return max even value from the interval
     */
    public static int getMaxEvenNumberFromInterval(final int intervalBegin, final int intervalEnd) {
        int maxEvenNumber = 0;
        for (int i = intervalBegin; i < intervalEnd; i++) {
            if (!isOddNumber(i) && i > maxEvenNumber) {
                maxEvenNumber = i;
            }
        }
        return maxEvenNumber;
    }

    /**
     * Builds a row of the Fibonacci numbers
     *
     * @param fibonacciSteps the number of steps left
     * @param leftNumber contains previous numbers and increase rightNumber
     * @param rightNumber the sum of two previous number
     * @return a last number of a row of the fibonacci numbers
     */
    public static double buildFibonacciNumbers(int fibonacciSteps, final int leftNumber, final int rightNumber) {
        // show a row and add each number to array of fibonacci numbers
        // if the number of steps left not equal 0 then call this function again
        // but decrease a number of the steps and transfer a sum of left and right numbers
        System.out.print(leftNumber + " ");
        fibonacciNumbers.add(leftNumber);
        if (fibonacciSteps == 0) {
            showPercentageEvenAndOdd();
            return rightNumber;
        } else {
            return buildFibonacciNumbers(--fibonacciSteps, rightNumber, leftNumber + rightNumber);
        }
    }

    /**
     * Finds maximum an odd and even numbers in the row of Fibonacci
     * @param fibonacci a row of Fibonacci numbers
     */
    public static void findMaxOddAndEven(final ArrayList<Integer> fibonacci) {
        int maxOdd = 0;
        int maxEven = 0;
        for (final Integer i : fibonacci) {
            if (isOddNumber(i) && i > maxOdd) {
                maxOdd = i;
            } else if (!isOddNumber(i) && i > maxEven) {
                maxEven = i;
            }
        }
        System.out.println("Max odd value: " + maxOdd);
        System.out.println("Max even value: " + maxEven);
    }

    /**
     * Show percentage of an even and odd numbers in the fibonacci row
     */
    public static void showPercentageEvenAndOdd() {
        // firstly finding a numbers of an odd numbers and even numbers
        // then find a percentage relation
        // after this clear out array of the fibonacci numbers
        int numberOfEvenNumbers = 0;
        int numberOfOddNumbers = 0;
        for (final Integer i : fibonacciNumbers) {
            if (isOddNumber(i)) {
                numberOfOddNumbers++;
            } else {
                numberOfEvenNumbers++;
            }
        }
        System.out.println("\nPercentage of odd number: "
                + numberOfOddNumbers * 100 / fibonacciNumbers.size() + "%");
        System.out.println("Percentage of even number: "
                + numberOfEvenNumbers * 100 / fibonacciNumbers.size() + "%");
        findMaxOddAndEven(fibonacciNumbers);
        fibonacciNumbers.clear();
    }

    /**
     * Provides the user with the ability to select the features
     * he wants to apply
     *
     * @param inputValue receive a user feedback
     * @param intervalBegin beginning of the interval
     * @param intervalEnd end of the interval
     */
    public static void showFunctionToUser(final Scanner inputValue, final int intervalBegin, final int intervalEnd) {
        short userFunctionSelection = 0;
        while (true) {
            System.out.println("Choose what do you want to use:\n"
                    + "1: Prints odd numbers from start to the end and even from end to start\n"
                    + "2: Prints the sum of odd and even numbers\n"
                    + "3: Prints Fibonacci numbers\n"
                    + "Default: Exit\n"
                    + "Enter number of function: ");
            userFunctionSelection = inputValue.nextShort();
            switch (userFunctionSelection) {
                case 1:
                    showOddAndEvenNumbers(intervalBegin, intervalEnd);
                    break;
                case 2:
                    showSumOddAndEvenNumbers(intervalBegin, intervalEnd);
                    break;
                case 3:
                    System.out.print("Build Fibonacci for max odd number: ");
                    buildFibonacciNumbers(getMaxOddNumberFromInterval(intervalBegin, intervalEnd), 0, 1);
                    System.out.print("\nBuilt Fibonacci for max even number: ");
                    buildFibonacciNumbers(getMaxEvenNumberFromInterval(intervalBegin, intervalEnd), 0, 1);
                    System.out.println("\nNow, you can enter your own fibonacci number: ");

                    final int userFibonacciNumber = inputValue.nextInt();
                    if (userFibonacciNumber >= 0) {
                        System.out.print("Build Fibonacci for " + userFibonacciNumber + ": ");
                        buildFibonacciNumbers(userFibonacciNumber, 0, 1);
                        System.out.println();
                    } else {
                        System.out.println("Fibonacci number should be bigger than 0!");
                    }
                    break;
                default:
                    return;
            }
        }
    }

    /**
     * Start application
     * @param args
     */
    public static void main(final String[] args) {
        // a user have to enter the beginning of the interval and end of this interval
        // provides protection from a fool
        int intervalBegin = 0;
        int intervalEnd = 0;
        final Scanner inputValue = new Scanner(System.in);
        System.out.println("Welcome");
        while (true) {
            System.out.println("Enter the interval of numbers [a : b]:");
            System.out.println("a: ");
            intervalBegin = inputValue.nextShort();
            System.out.println("b: ");
            intervalEnd = inputValue.nextShort();

            if (intervalBegin < intervalEnd) {
                break;
            } else {
                System.out.println("Please, a should be lower than b!");
            }
        }
        showFunctionToUser(inputValue, intervalBegin, intervalEnd);
    }
}
